<?php

namespace App\Models;

use Arrilot\BitrixModels\Models\UserModel;

class User extends UserModel
{
    public $timestamps = false;

    public function getPhoto()
    {
        return $this['PERSONAL_PHOTO'];
    }

    public function getFullName()
    {
        return $this['FULL_NAME'];
    }

    public function getFullNameAttribute()
    {
        return $this['NAME'].' '.$this['LAST_NAME'];
    }

    public function getLogin()
    {
        return $this['LOGIN'];
    }

    public function getLastName()
    {
        return $this['LAST_NAME'];
    }

    public function getName()
    {
        return $this['NAME'];
    }

    public function getProfession()
    {
        return $this["PERSONAL_PROFESSION"];
    }
    
}