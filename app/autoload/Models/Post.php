<?php

namespace App\Models;

use Arrilot\BitrixModels\Models\ElementModel;

/**
 * Class Post
 * @package App\Models
 *
 * @property-read User $user
 */
class Post extends ElementModel
{
    /**
     * Corresponding iblock id.
     *
     * @return string
     */
    const IBLOCK_CODE = 'posts';

    public $timestamps = false;

    public static function iblockId(): int
    {
        return iblock_id(static::IBLOCK_CODE);
    }

    public function getDateCreate(): string
    {
        return $this['DATE_CREATE'];
    }

    public function getDateCreateAttribute($dateCreate)
    {
        return FormatDate('j F Y \в h:i', MakeTimeStamp($dateCreate));
    }

    public function getId()
    {
        return $this['ID'];
    }

    public function user()
    {
        return $this->hasOne(User::class, 'ID', 'PROPERTY_AUTHOR_ID_VALUE');
    }

    public function getDetailUrl(): string
    {
        return $this['DETAIL_PAGE_URL'];
    }

    public function getTitle()
    {
        return $this['NAME'];
    }

    public function getBody()
    {
        return $this["DETAIL_TEXT"];
    }

    public function getPlusVotes()
    {
        return $this['PROPERTY_PLUS_VOTES_VALUE'];
    }

    public function getMinusVotes()
    {
        return $this['PROPERTY_MINUS_VOTES_VALUE'];
    }

    public function getRating()
    {
        return $this['PROPERTY_RATING_VALUE'];
    }

    public function getPreview()
    {
        return $this["PREVIEW_TEXT"];
    }
}