<?php

namespace App\Api\Internal;

use App\Api\BaseController;
use App\Models\Post;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class VoteController extends BaseController
{
    public function updatePostRating($postId)
    {
        $request = (object) $this->request->getParsedBody();

        /** @var Post $post */
        $post = Post::select(['ID', 'PROPERTY_PLUS_VOTES', 'PROPERTY_MINUS_VOTES', 'PROPERTY_RATING'])
                    ->getById($postId);

        if(!isset($post)) {
            return $this->errorInternalError();
        }

        switch ($request->vote) {
            case 'plus':
                $post['PROPERTY_PLUS_VOTES_VALUE'] += 1;
                break;
            case 'minus':
                $post['PROPERTY_MINUS_VOTES_VALUE'] += 1;
                break;
        }

        $post['PROPERTY_RATING_VALUE'] = $post['PROPERTY_PLUS_VOTES_VALUE'] - $post['PROPERTY_MINUS_VOTES_VALUE'];

        $post->save(['PROPERTY_PLUS_VOTES_VALUE', 'PROPERTY_MINUS_VOTES_VALUE', 'PROPERTY_RATING_VALUE']);

        $response = json_encode(
            array(
                'plusVotes' => (int) $post['PROPERTY_PLUS_VOTES_VALUE'],
                'minusVotes' => (int) $post['PROPERTY_MINUS_VOTES_VALUE'],
                'rating' => (int) $post['PROPERTY_RATING_VALUE']
            )
        );

        return $this->response->write($response)->withHeader('Content-Type', 'application/json');
    }
}