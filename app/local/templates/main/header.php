<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<!DOCTYPE html>
<html lang="ru" class="no-js">
<head>
<!--    <meta name="csrf-token" content="--><?//= bitrix_sessid() ?><!--">-->

    <title><?$APPLICATION->ShowTitle();?></title>
    <?$APPLICATION->ShowHead();?>
    <link rel="icon" href="<?=SITE_TEMPLATE_PATH?>/ico/favicon_bx.png">

    <?
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/foundation.min.css");
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/foundation.css");

    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/vendor/jquery.js');
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/vendor/foundation.min.js');
    ?>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta content='width=1024' name='viewport'>
    <title>PHP – Скриптовый язык общего назначения / Хабр</title>

    <meta name="description" content="PHP (англ. PHP: Hypertext Preprocessor — «PHP: препроцессор гипертекста»; первоначально Personal Home Page Tools — «Инструменты для создания персональных веб-страниц»; произносится пи-эйч-пи) — скриптовый язык общего назначения, интенсивно применяемый для разработки веб-приложений. В настоящее время поддерживается подавляющим большинством хостинг-провайдеров и является одним из лидеров среди языков, применяющихся для создания динамических веб-сайтов. Язык и его интерпретатор разрабатываются группой энтузиастов в рамках проекта с открытым кодом. Проект распространяется под собственной лицензией, несовместимой с GNU GPL." />

    <meta name="keywords" content="php, hypertext preprocessor, personal home page tools, laravel, symfony, дайджест, yii, web-разработка, ссылки, новости, подборка, php-дайджест, framework, php 7, javascript, yii 2, wordpress, composer, cms, phpstorm, magento, mysql" />

    <link rel="canonical" href="https://habr.com/ru/hub/php/"/>

    <link rel="alternate" hreflang="ru" href="https://habr.com/ru/hub/php/" />
    <link rel="alternate" hreflang="en" href="https://habr.com/en/hub/php/" />
    <meta property="fb:app_id" content="444736788986613" />
    <meta property="og:type" content="website"/>
    <meta property="fb:pages" content="472597926099084"/>
    <meta property="og:site_name" content="Хабр" />
    <link rel="image_src" href="https://habr.com/images/habr_ru.png" />
    <meta property="og:image" content="https://habr.com/images/habr_ru.png" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="628" />
    <meta property="og:title" content="PHP – Скриптовый язык общего назначения / Хабр"/>
    <meta property="og:description" content="PHP (англ. PHP: Hypertext Preprocessor — «PHP: препроцессор гипертекста»; первоначально Personal Home Page Tools — «Инструменты для создания персональных веб-страниц»; произносится пи-эйч-пи) — скриптовый язык общего назначения, интенсивно применяемый для разработки веб-приложений. В настоящее время поддерживается подавляющим большинством хостинг-провайдеров и является одним из лидеров среди языков, применяющихся для создания динамических веб-сайтов. Язык и его интерпретатор разрабатываются группой энтузиастов в рамках проекта с открытым кодом. Проект распространяется под собственной лицензией, несовместимой с GNU GPL."/>



    <meta name='yandex-verification' content='71593b225aeafc4e' />
    <meta name='referrer' content='unsafe-url' />
    <meta name="pocket-site-verification" content="ed24b2b9721edf0a282c5b4a3232c4" />
    <meta name="biu" content="https://dr.habracdn.net/habr/5f2c1748/images/">

    <style type="text/css">
        @font-face{font-family:'Fira Sans';font-style:normal;font-weight:500;src:url(https://dr.habracdn.net/habr/5f2c1748/fonts/FiraSans/firaSans-medium.eot);src:local("Fira Sans Medium"),local("FiraSans-Medium"),url(https://dr.habracdn.net/habr/5f2c1748/fonts/FiraSans/firaSans-medium.eot?#iefix) format("embedded-opentype"),url(https://dr.habracdn.net/habr/5f2c1748/fonts/FiraSans/firaSans-medium.woff2) format("woff2"),url(https://dr.habracdn.net/habr/5f2c1748/fonts/FiraSans/firaSans-medium.woff) format("woff"),url(https://dr.habracdn.net/habr/5f2c1748/fonts/FiraSans/firaSans-medium.ttf) format("truetype")}
    </style>

    <link href="https://dr.habracdn.net/habr/5f2c1748/styles/main.bundle.css" rel="stylesheet" media="all" />





    <meta name='yandex-verification' content='67d46b975fa41645' />

    <link rel="apple-touch-icon" sizes="180x180" href="https://dr.habracdn.net/habr/5f2c1748/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://dr.habracdn.net/habr/5f2c1748/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://dr.habracdn.net/habr/5f2c1748/images/favicon-16x16.png">
    <link rel="manifest" href="https://dr.habracdn.net/habr/5f2c1748/site.webmanifest">
    <link rel="mask-icon" href="https://dr.habracdn.net/habr/5f2c1748/images/safari-pinned-tab.svg" color="#77a2b6">
    <meta name="application-name" content="Хабр"/>
    <meta name="msapplication-TileColor" content="#77a2b6">
    <meta name="theme-color" content="#77a2b6">


    <link title="PHP – Скриптовый язык общего назначения" type="application/rss+xml" rel="alternate" href="https://habr.com/ru/rss/hub/php/all/?fl=ru"/>

    <script>/* Font Face Observer v2.0.13 - © Bram Stein. License: BSD-3-Clause */(function(){'use strict';var f,g=[];function l(a){g.push(a);1==g.length&&f()}function m(){for(;g.length;)g[0](),g.shift()}f=function(){setTimeout(m)};function n(a){this.a=p;this.b=void 0;this.f=[];var b=this;try{a(function(a){q(b,a)},function(a){r(b,a)})}catch(c){r(b,c)}}var p=2;function t(a){return new n(function(b,c){c(a)})}function u(a){return new n(function(b){b(a)})}function q(a,b){if(a.a==p){if(b==a)throw new TypeError;var c=!1;try{var d=b&&b.then;if(null!=b&&"object"==typeof b&&"function"==typeof d){d.call(b,function(b){c||q(a,b);c=!0},function(b){c||r(a,b);c=!0});return}}catch(e){c||r(a,e);return}a.a=0;a.b=b;v(a)}}
            function r(a,b){if(a.a==p){if(b==a)throw new TypeError;a.a=1;a.b=b;v(a)}}function v(a){l(function(){if(a.a!=p)for(;a.f.length;){var b=a.f.shift(),c=b[0],d=b[1],e=b[2],b=b[3];try{0==a.a?"function"==typeof c?e(c.call(void 0,a.b)):e(a.b):1==a.a&&("function"==typeof d?e(d.call(void 0,a.b)):b(a.b))}catch(h){b(h)}}})}n.prototype.g=function(a){return this.c(void 0,a)};n.prototype.c=function(a,b){var c=this;return new n(function(d,e){c.f.push([a,b,d,e]);v(c)})};
            function w(a){return new n(function(b,c){function d(c){return function(d){h[c]=d;e+=1;e==a.length&&b(h)}}var e=0,h=[];0==a.length&&b(h);for(var k=0;k<a.length;k+=1)u(a[k]).c(d(k),c)})}function x(a){return new n(function(b,c){for(var d=0;d<a.length;d+=1)u(a[d]).c(b,c)})};window.Promise||(window.Promise=n,window.Promise.resolve=u,window.Promise.reject=t,window.Promise.race=x,window.Promise.all=w,window.Promise.prototype.then=n.prototype.c,window.Promise.prototype["catch"]=n.prototype.g);}());

        (function(){function l(a,b){document.addEventListener?a.addEventListener("scroll",b,!1):a.attachEvent("scroll",b)}function m(a){document.body?a():document.addEventListener?document.addEventListener("DOMContentLoaded",function c(){document.removeEventListener("DOMContentLoaded",c);a()}):document.attachEvent("onreadystatechange",function k(){if("interactive"==document.readyState||"complete"==document.readyState)document.detachEvent("onreadystatechange",k),a()})};function r(a){this.a=document.createElement("div");this.a.setAttribute("aria-hidden","true");this.a.appendChild(document.createTextNode(a));this.b=document.createElement("span");this.c=document.createElement("span");this.h=document.createElement("span");this.f=document.createElement("span");this.g=-1;this.b.style.cssText="max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";this.c.style.cssText="max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";
            this.f.style.cssText="max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";this.h.style.cssText="display:inline-block;width:200%;height:200%;font-size:16px;max-width:none;";this.b.appendChild(this.h);this.c.appendChild(this.f);this.a.appendChild(this.b);this.a.appendChild(this.c)}
            function t(a,b){a.a.style.cssText="max-width:none;min-width:20px;min-height:20px;display:inline-block;overflow:hidden;position:absolute;width:auto;margin:0;padding:0;top:-999px;white-space:nowrap;font-synthesis:none;font:"+b+";"}function y(a){var b=a.a.offsetWidth,c=b+100;a.f.style.width=c+"px";a.c.scrollLeft=c;a.b.scrollLeft=a.b.scrollWidth+100;return a.g!==b?(a.g=b,!0):!1}function z(a,b){function c(){var a=k;y(a)&&a.a.parentNode&&b(a.g)}var k=a;l(a.b,c);l(a.c,c);y(a)};function A(a,b){var c=b||{};this.family=a;this.style=c.style||"normal";this.weight=c.weight||"normal";this.stretch=c.stretch||"normal"}var B=null,C=null,E=null,F=null;function G(){if(null===C)if(J()&&/Apple/.test(window.navigator.vendor)){var a=/AppleWebKit\/([0-9]+)(?:\.([0-9]+))(?:\.([0-9]+))/.exec(window.navigator.userAgent);C=!!a&&603>parseInt(a[1],10)}else C=!1;return C}function J(){null===F&&(F=!!document.fonts);return F}
            function K(){if(null===E){var a=document.createElement("div");try{a.style.font="condensed 100px sans-serif"}catch(b){}E=""!==a.style.font}return E}function L(a,b){return[a.style,a.weight,K()?a.stretch:"","100px",b].join(" ")}
            A.prototype.load=function(a,b){var c=this,k=a||"BESbswy",q=0,D=b||3E3,H=(new Date).getTime();return new Promise(function(a,b){if(J()&&!G()){var M=new Promise(function(a,b){function e(){(new Date).getTime()-H>=D?b():document.fonts.load(L(c,'"'+c.family+'"'),k).then(function(c){1<=c.length?a():setTimeout(e,25)},function(){b()})}e()}),N=new Promise(function(a,c){q=setTimeout(c,D)});Promise.race([N,M]).then(function(){clearTimeout(q);a(c)},function(){b(c)})}else m(function(){function u(){var b;if(b=-1!=
                f&&-1!=g||-1!=f&&-1!=h||-1!=g&&-1!=h)(b=f!=g&&f!=h&&g!=h)||(null===B&&(b=/AppleWebKit\/([0-9]+)(?:\.([0-9]+))/.exec(window.navigator.userAgent),B=!!b&&(536>parseInt(b[1],10)||536===parseInt(b[1],10)&&11>=parseInt(b[2],10))),b=B&&(f==v&&g==v&&h==v||f==w&&g==w&&h==w||f==x&&g==x&&h==x)),b=!b;b&&(d.parentNode&&d.parentNode.removeChild(d),clearTimeout(q),a(c))}function I(){if((new Date).getTime()-H>=D)d.parentNode&&d.parentNode.removeChild(d),b(c);else{var a=document.hidden;if(!0===a||void 0===a)f=e.a.offsetWidth,
                g=n.a.offsetWidth,h=p.a.offsetWidth,u();q=setTimeout(I,50)}}var e=new r(k),n=new r(k),p=new r(k),f=-1,g=-1,h=-1,v=-1,w=-1,x=-1,d=document.createElement("div");d.dir="ltr";t(e,L(c,"sans-serif"));t(n,L(c,"serif"));t(p,L(c,"monospace"));d.appendChild(e.a);d.appendChild(n.a);d.appendChild(p.a);document.body.appendChild(d);v=e.a.offsetWidth;w=n.a.offsetWidth;x=p.a.offsetWidth;I();z(e,function(a){f=a;u()});t(e,L(c,'"'+c.family+'",sans-serif'));z(n,function(a){g=a;u()});t(n,L(c,'"'+c.family+'",serif'));
                z(p,function(a){h=a;u()});t(p,L(c,'"'+c.family+'",monospace'))})})};"object"===typeof module?module.exports=A:(window.FontFaceObserver=A,window.FontFaceObserver.prototype.load=A.prototype.load);}());

        (function( w ){
            if( w.document.documentElement.className.indexOf( "fonts-loaded" ) > -1 ){ return; }

            var html = document.documentElement;
            var FS500 = new w.FontFaceObserver("Fira Sans", { weight: 500 });

            FS500.load().then(function() {
                html.classList.add('fonts-loaded');
                sessionStorage.fontsLoaded = true;
                console.log('FS500-loaded');
            }).catch(function () {
                sessionStorage.fontsLoaded = false;
                console.log('FS500-unloaded');
            });

            if (sessionStorage.fontsLoaded) {
                html.classList.add('fonts-loaded');
            }
        }(this));
    </script>
    <script src="https://dr.habracdn.net/habr/5f2c1748/javascripts/libs/jquery-1.8.3.min.js"></script>
    <script src="https://dr.habracdn.net/habr/5f2c1748/javascripts/libs/chance.min.js"></script>
    <script src="https://dr.habracdn.net/habr/5f2c1748/javascripts/libs/html2canvas.min.js"></script>
    <script src="https://unpkg.com/pure-md5@latest/lib/index.js"></script>
    <script async src="https://cdn.onthe.io/io.js/Tn4LrERWU1qX"></script>
    <script>
        window._io_config = window._io_config || {};
        window._io_config["0.2.0"] = window._io_config["0.2.0"] || [];
        window._io_config["0.2.0"].push({
            "page_url": "https:\/\/habr.com\/ru\/hub\/php\/",
            "page_url_canonical": "https:\/\/habr.com\/ru\/hub\/php\/",
            "page_title": "PHP – Скриптовый язык общего назначения \/ Хабр",
            "page_type": "default",
            "page_language": "ru"
        });
    </script>

    <script>
        window.create_callback_for_blocked = function(key) {
            return function() {
                googletag.cmd.push(function () {
                    googletag.display(key);
                });
            }
        }

        window.habr_blockers_checker = new function() {
            var result;
            var callbacksQueue = [];
            var calledOnce = false;

            function fireCallbacks() {
                callbacksQueue.forEach(function(callback) {
                    callback(result);
                });
                callbacksQueue = [];
            }

            this.detect = function(imgUrl, callback) {
                var checksRemain = 2;
                var detected = false;
                var error1 = false;
                var error2 = false;

                if (typeof callback !== 'function') {
                    return;
                };

                callbacksQueue.push(callback);

                if (typeof result !== 'undefined') {
                    fireCallbacks();
                };

                if (calledOnce) {
                    return;
                }
                calledOnce = true;

                imgUrl += '?ch=*&rn=*';

                function beforeCheck(timeout) {
                    if (checksRemain === 0 || timeout > 1E3) {
                        result = checksRemain === 0 && detected;
                        fireCallbacks();
                    } else {
                        setTimeout(function() {
                            beforeCheck(timeout * 2)
                        }, timeout * 2);
                    }
                }

                function checkImages() {
                    if (--checksRemain) {
                        return;
                    };
                    detected = !error1 && error2;
                }

                var random = Math.random() * 11;

                var img1 = new Image;
                img1.onload = checkImages;
                img1.onerror = function() {
                    error1 = true;
                    checkImages()
                };
                img1.src = imgUrl.replace(/\*/, 1).replace(/\*/, random);

                var img2 = new Image;
                img2.onload = checkImages;
                img2.onerror = function() {
                    error2 = true;
                    checkImages()
                };
                img2.src = imgUrl.replace(/\*/, 2).replace(/\*/, random);

                beforeCheck(250, callback)
            };

            this.detectWrapper = function(callback) {
                return this.detect('/images/px.gif', callback);
            };
        };

        window.display_dfp_slot = function(key) {
            if (window.habr_blockers_checker) {
                window.habr_blockers_checker.detectWrapper(window.create_callback_for_blocked(key));
            } else {
                window.create_callback_for_blocked(key)();
            }
        };
    </script>


    <script src="https://dr.habracdn.net/habr/5f2c1748/javascripts/libs/raven.min.js"></script>
    <script>Raven.config('https://830576edd4b7478086093f693a5a0df5@s.tmtm.ru/37', {
            maxBreadcrumbs: 50,
            sampleRate: 0.5,
            whitelistUrls: [/https?:\/\/((www)\.)?(m\.)?habr\.com/],
        }).install()</script>

    <script src="https://dr.habracdn.net/habr/5f2c1748/javascripts/_parts/advertise.js"></script>
    <script src="https://dr.habracdn.net/habr/5f2c1748/javascripts/_parts/adriver.js"></script>
    <script src="https://www.googletagservices.com/tag/js/gpt.js" async></script>
    <script src="https://static.criteo.net/js/ld/publishertag.js" async></script>

    <script>
        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];
    </script>

    <script>
        // TODO: move this code to module
        //       move dot in chains to start of line and use one indent level:
        //               ```
        //                  start()
        //                    .chainEnd()
        //               ```

        var ENABLE_YANDEX_ADS = false;

        function display_yandex_rtb(yandexId, divId) {
            $('#' + divId).show();
            (function(w, d, n, s, t) {
                w[n] = w[n] || [];
                w[n].push(function() {
                    Ya.Context.AdvManager.render({
                        blockId: yandexId,
                        renderTo: divId,
                        async: true
                    });
                });
                t = d.getElementsByTagName('script')[0];
                s = d.createElement('script');
                s.type = 'text/javascript';
                s.src = '//an.yandex.ru/system/context.js';
                s.async = true;
                t.parentNode.insertBefore(s, t);
            })(this, this.document, 'yandexContextAsyncCallbacks');
        }

        function listen_googletag_and_render_yandex(yandexId, targetDivId) {
            if (!window.googletag || !window.googletag.cmd) {
                return;
            }
            window.googletag.cmd.push(function() {
                if (!window.googletag.pubads) {
                    return;
                }
                if (ENABLE_YANDEX_ADS) {
                    window.googletag.pubads()
                        .addEventListener('slotRenderEnded', function(event) {
                            var divId = event.slot.getSlotElementId();
                            if (divId === targetDivId && event.isEmpty) {
                                window.display_yandex_rtb(yandexId, divId);
                            }
                        });
                }
            });
        }

        function getRoxotItem(storageKey, config) {
            if (config) {
                var { randomVal, returnCode } = config;
            } else {
                var randomVal = 100,
                    returnCode = '101';
            }
            try {
                var groupId = localStorage.getItem(storageKey);
                if (groupId === null) {
                    groupId = 1 + Math.floor(Math.random() * randomVal);
                    localStorage.setItem(storageKey, groupId);
                }
                return '' + groupId;
            }
            catch (e) {
                return returnCode;
            }
        }

        function getRoxotEvent() {
            return '' + (1 + Math.floor(Math.random() * 100));
        }

        function leftpad(str, len, ch) {
            str = String(str);

            var i = -1;

            if (!ch && ch !== 0) {
                ch = ' ';
            };

            len = len - str.length;

            while (++i < len) {
                str = ch + str;
            }

            return str;
        }

        var WEEK_DAYS = ['su', 'mo', 'tu', 'we', 'th', 'fr', 'sa'];

        function initializeDFP(usesABP) {
            var currentDate = new Date();
            var hours = currentDate.getHours();
            var hoursStr = leftpad(hours, 2, '0');
            var weekDayCode = WEEK_DAYS[currentDate.getDay()];
            googletag.cmd.push(function() {
                if (usesABP) {
                    googletag.defineSlot('/235032688/HH/HH02_inpage_wide', [[1, 1], [780, 130], [780, 200], [780, 180], [780, 110], [728, 90]], 'div-gpt-hh-inpage-wide').addService(googletag.pubads());
                    window.listen_googletag_and_render_yandex('R-A-149298-32', 'div-gpt-hh-inpage-wide');
                } else {
                    googletag.defineSlot('/235032688/HH/HH02_inpage_wide', [[1, 1], [780, 200], [780, 130], [780, 180], [780, 110], [728, 90], [780, 195], [780, 210], [780, 260]], 'div-gpt-hh-inpage-wide').addService(googletag.pubads());
                    window.listen_googletag_and_render_yandex('R-A-149298-36', 'div-gpt-hh-inpage-wide');
                }
                if (usesABP) {
                    googletag.defineSlot('/235032688/HH/HH01_ATF_Poster', [[1, 1], [300, 500], [300, 250], [300, 300], [300, 400], [300, 200], [300, 100], [240, 400]], 'div-gpt-hh-atf').addService(googletag.pubads());
                    window.listen_googletag_and_render_yandex('R-A-149298-31', 'div-gpt-hh-atf');
                } else {
                    googletag.defineSlot('/235032688/HH/HH01_ATF_Poster', [[1, 1], [300, 500], [300, 600], [300, 250], [300, 300], [300, 400], [300, 200], [300, 150], [300, 100], [240, 400]], 'div-gpt-hh-atf').addService(googletag.pubads());
                    window.listen_googletag_and_render_yandex('R-A-149298-35', 'div-gpt-hh-atf');
                }

                googletag.pubads().enableSingleRequest();
                if (!usesABP) {
                    googletag.pubads().disableInitialLoad(); //We will wait for the Criteo Bidder Call before calling the adserver
                }
                googletag.pubads().collapseEmptyDivs();
                googletag.pubads().
                setTargeting('roxot-group-id', getRoxotItem('roxot-gid', { randomVal: 10000, returnCode: '10001'})).
                setTargeting('roxot-sector-id', getRoxotItem('roxot-sid')).
                setTargeting('roxot-deep', getRoxotItem('roxot-deep')).
                setTargeting('roxot-event-group-id', getRoxotEvent()).
                setTargeting('roxot-event', getRoxotEvent()).
                setTargeting('roxot-event-deep', getRoxotEvent()).
                setTargeting('roxot-minutes', (new Date).getUTCMinutes().toString()).
                setTargeting('roxot-hours', (new Date).getUTCHours().toString()).
                setTargeting('roxot-day', (new Date).getUTCDay().toString()).
                setTargeting('chr', hoursStr).
                setTargeting('cwd', weekDayCode).
                setTargeting('aa', (usesABP) ? 'yes' : 'no').
                setTargeting('path', window.location.pathname + window.location.search).
                setTargeting('feedtype', ["all_all","posts"]).
                setTargeting('cat', ["h_php","f_develop"]).
                setTargeting('dev', [0]).
                setTargeting('user', ["guest"]).
                setTargeting('pagetype', ["feed"]);
                googletag.enableServices();

                if (!usesABP) {
                    window.Criteo = window.Criteo || {};
                    window.Criteo.events = window.Criteo.events || [];
                    var launchAdServer = function () {
                        googletag.cmd.push(function () {
                            Criteo.SetDFPKeyValueTargeting(); //This will append Criteo keywords to the adserver call
                            googletag.pubads().refresh(); //This will trigger the adserver call
                        });
                    };
                    Criteo.events.push(function () {
                        var adUnits = {
                            'placements': [
                                {'slotid': 'div-gpt-hh-inpage-wide', 'zoneid': 1173491}, // TMTM - RU - CDB - SA - Habr - 780x180
                                {'slotid': 'div-gpt-hh-atf', 'zoneid': 1173489}, // TMTM - RU - CDB - SA - Habr - 300x600
                            ]
                        };
                        // Define the price band range
                        Criteo.SetLineItemRanges('0..599:1;600..1398:2;1400..3000:5');
                        // Call Criteo and execute the callback function for a given timeout
                        Criteo.RequestBids(adUnits, launchAdServer, 750);
                    });
                }
            });
        }
        if (window.habr_blockers_checker) {
            window.habr_blockers_checker.detectWrapper(initializeDFP);
        } else {
            initializeDFP(false);
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            if (!$.cookie('split201901')) {
                $.cookie(
                    'split201901',
                    'A',
                    {
                        expires: 3 * 7,
                        domain: 'habr.com',
                        path: '/',
                        secure: true
                    }
                );
            }
        });
    </script>


</head>

<body class="nl">

<?$APPLICATION->ShowPanel();?>

<div class="layout">
<div class="layout__row layout__row_services">
    <div id="TMpanel">
        <div class="container">
            <div class="logo-wrapper">
                <a href="https://habr.com/ru/" class="logo" title="">
                    <svg width="62" height="24" viewBox="0 0 62 24" xmlns="http://www.w3.org/2000/svg">
                        <path d="M16.875 19L11.075 10.225L16.825 1.4H12.6L8.75 7.4L4.94999 1.4H0.574994L6.32499 10.15L0.524994 19H4.79999L8.64999 12.975L12.525 19H16.875Z" />
                        <path d="M24.2607 5.775C20.8857 5.775 18.9607 7.625 18.6107 9.85H22.0107C22.2107 9.175 22.8607 8.6 24.1107 8.6C25.3357 8.6 26.2357 9.225 26.2357 10.425V11.025H23.4107C20.1107 11.025 18.1107 12.55 18.1107 15.2C18.1107 17.8 20.1107 19.3 22.6107 19.3C24.2857 19.3 25.6357 18.65 26.4357 17.6V19H29.8107V10.55C29.8107 7.4 27.5857 5.775 24.2607 5.775ZM23.6107 16.475C22.4857 16.475 21.7607 15.925 21.7607 15.025C21.7607 14.1 22.5607 13.55 23.6857 13.55H26.2357V14.125C26.2357 15.625 25.0107 16.475 23.6107 16.475Z" />
                        <path d="M39.925 6.3C38.125 6.3 36.65 6.95 35.7 8.275C35.95 5.85 36.925 4.65 39.375 4.275L44.3 3.55V0.375L39.025 1.25C33.925 2.1 32.35 5.5 32.35 11.175C32.35 16.275 34.825 19.3 39.2 19.3C43.125 19.3 45.55 16.3 45.55 12.7C45.55 8.825 43.3 6.3 39.925 6.3ZM39.025 16.25C37.125 16.25 36.075 14.725 36.075 12.675C36.075 10.7 37.175 9.275 39.05 9.275C40.875 9.275 41.9 10.75 41.9 12.7C41.9 14.65 40.9 16.25 39.025 16.25Z" />
                        <path d="M55.2855 5.775C53.3855 5.775 52.1605 6.6 51.5105 7.575V6.075H48.0105V23.775H51.6605V17.75C52.3105 18.65 53.5355 19.3 55.1855 19.3C58.3605 19.3 60.8855 16.8 60.8855 12.55C60.8855 8.225 58.3605 5.775 55.2855 5.775ZM54.4105 16.15C52.7105 16.15 51.5855 14.775 51.5855 12.6V12.5C51.5855 10.325 52.7105 8.925 54.4105 8.925C56.1105 8.925 57.2105 10.35 57.2105 12.55C57.2105 14.75 56.1105 16.15 54.4105 16.15Z" />
                    </svg>

                </a>


                <span class="projects-dropdown" id="dropdown-control">
      <svg width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M5.70711 5.70711C5.31658 6.09763 4.68342 6.09763 4.29289 5.70711L0.292892 1.70711C-0.097632 1.31658 -0.0976319 0.683417 0.292892 0.292893C0.683417 -0.0976308 1.31658 -0.0976308 1.70711 0.292893L5 3.58579L8.29289 0.292894C8.68342 -0.0976301 9.31658 -0.0976301 9.70711 0.292894C10.0976 0.683418 10.0976 1.31658 9.70711 1.70711L5.70711 5.70711Z" />
</svg>

    </span>
                <div class="dropdown hidden" id="dropdown">
                    <div class="dropdown-heading">
                        Все сервисы Хабра
                    </div>
                    <a class="service" href="/">
                        <div class="service-title">
                            <svg width="52" height="22" viewBox="0 0 52 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M13.86 17.0001L8.514 9.23405L13.816 1.51205H11.528L7.26 7.76005L2.97 1.51205H0.659997L5.984 9.19005L0.615997 17.0001H2.926L7.194 10.6861L11.528 17.0001H13.86Z" fill="#333333"/>
                                <path d="M20.181 5.67005C17.651 5.67005 16.111 7.10005 15.759 8.81605H17.585C17.893 8.00205 18.685 7.34205 20.137 7.34205C21.567 7.34205 22.645 8.11205 22.645 9.65205V10.4001H19.697C16.991 10.4001 15.341 11.6981 15.341 13.8321C15.341 15.9441 17.013 17.2421 19.169 17.2421C20.577 17.2421 21.897 16.7141 22.733 15.6801V17.0001H24.515V9.65205C24.515 7.16605 22.777 5.67005 20.181 5.67005ZM19.411 15.5921C18.179 15.5921 17.255 14.9761 17.255 13.8101C17.255 12.6221 18.289 12.0061 19.807 12.0061H22.645V12.8201C22.645 14.5141 21.171 15.5921 19.411 15.5921Z" fill="#333333"/>
                                <path d="M32.9685 5.91205C31.2965 5.91205 29.9545 6.55005 29.1185 7.78205C29.3825 5.27405 30.3945 3.88805 32.9025 3.42605L36.9505 2.67805V0.852051L32.5065 1.75405C28.7665 2.52405 27.2045 4.96605 27.2045 10.5101C27.2045 14.8001 29.4045 17.2421 32.7925 17.2421C36.0265 17.2421 38.0505 14.7121 38.0505 11.5441C38.0505 7.91405 35.8285 5.91205 32.9685 5.91205ZM32.7045 15.5261C30.6145 15.5261 29.2065 13.9641 29.2065 11.4561C29.2065 8.97005 30.7685 7.60605 32.7265 7.60605C34.7725 7.60605 36.1145 9.23405 36.1145 11.5441C36.1145 13.8541 34.7945 15.5261 32.7045 15.5261Z" fill="#333333"/>
                                <path d="M46.1431 5.67005C44.2291 5.67005 43.0631 6.55005 42.5131 7.49605V5.91205H40.6871V21.2021H42.5791V15.6361C43.1071 16.4501 44.2951 17.2421 46.0771 17.2421C48.6291 17.2421 51.1371 15.3721 51.1371 11.4561C51.1371 7.56205 48.6511 5.67005 46.1431 5.67005ZM45.9011 15.5261C43.8551 15.5261 42.5131 13.9641 42.5131 11.5001V11.4121C42.5131 8.94805 43.8551 7.38605 45.9011 7.38605C47.8811 7.38605 49.2011 9.03605 49.2011 11.4561C49.2011 13.8761 47.8811 15.5261 45.9011 15.5261Z" fill="#333333"/>
                            </svg>
                        </div>
                        <p class="service-description">
                            Сообщество IT-специалистов
                        </p>
                    </a>
                    <a class="service" href="https://qna.habr.com?utm_source=habr&utm_medium=habr_top_panel">
                        <h4 class="service-title">
                            <svg width="46" height="18" viewBox="0 0 46 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M14.4 8.25602C14.4 3.65802 11.606 0.27002 7.27201 0.27002C2.93801 0.27002 0.144012 3.65802 0.144012 8.25602C0.144012 12.854 2.93801 16.242 7.27201 16.242C8.41601 16.242 9.45001 16.022 10.352 15.604L11.518 17.342H13.696L11.848 14.614C13.476 13.184 14.4 10.918 14.4 8.25602ZM7.27201 14.416C4.10401 14.416 2.14601 11.864 2.14601 8.25602C2.14601 4.64802 4.10401 2.09602 7.27201 2.09602C10.44 2.09602 12.398 4.64802 12.398 8.25602C12.398 10.236 11.826 11.908 10.77 12.986L9.64801 11.314H7.47001L9.29601 14.02C8.70201 14.284 8.02001 14.416 7.27201 14.416Z" fill="#333333"/>
                                <path d="M30.965 16L27.973 12.766L30.921 9.11402H28.699L26.829 11.534L23.331 7.77202C25.377 6.80402 26.455 5.59402 26.455 3.85602C26.455 1.78802 24.871 0.27002 22.583 0.27002C20.207 0.27002 18.535 1.89802 18.535 3.87802C18.535 5.19802 19.305 6.12202 20.163 7.00202L20.427 7.26602C17.985 8.25602 16.753 9.73002 16.753 11.732C16.753 14.196 18.667 16.242 21.835 16.242C23.749 16.242 25.311 15.494 26.565 14.24L26.763 14.042L28.567 16H30.965ZM22.539 1.92002C23.705 1.92002 24.629 2.62402 24.629 3.83402C24.629 5.00002 23.793 5.90202 22.187 6.62802L21.571 6.01202C21.109 5.55002 20.405 4.82402 20.405 3.81202C20.405 2.66802 21.329 1.92002 22.539 1.92002ZM21.945 14.504C19.877 14.504 18.755 13.316 18.755 11.666C18.755 10.258 19.591 9.20202 21.593 8.43202L25.641 12.832L25.509 12.964C24.541 13.976 23.309 14.504 21.945 14.504Z" fill="#333333"/>
                                <path d="M43.5619 16H45.6739L39.8219 0.512019H37.7979L31.9459 16H34.0579L35.5539 11.908H42.0439L43.5619 16ZM38.7879 2.97602L41.3839 10.104H36.2139L38.7879 2.97602Z" fill="#333333"/>
                            </svg>
                        </h4>
                        <p class="service-description">
                            Ответы на&nbsp;любые вопросы об&nbsp;IT
                        </p>
                    </a>
                    <a class="service" href="https://career.habr.com?utm_source=habr&utm_medium=habr_top_panel">
                        <div class="service-title">
                            <svg width="84" height="21" viewBox="0 0 84 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M12.442 16L4.96201 7.92596L12.31 0.511963H9.78001L2.894 7.41996V0.511963H0.936005V16H2.894V8.60796L9.84601 16H12.442Z" fill="#333333"/>
                                <path d="M18.3646 4.66996C15.8346 4.66996 14.2946 6.09996 13.9426 7.81596H15.7686C16.0766 7.00196 16.8686 6.34196 18.3206 6.34196C19.7506 6.34196 20.8286 7.11196 20.8286 8.65196V9.39996H17.8806C15.1746 9.39996 13.5246 10.698 13.5246 12.832C13.5246 14.944 15.1966 16.242 17.3526 16.242C18.7606 16.242 20.0806 15.714 20.9166 14.68V16H22.6986V8.65196C22.6986 6.16596 20.9606 4.66996 18.3646 4.66996ZM17.5946 14.592C16.3626 14.592 15.4386 13.976 15.4386 12.81C15.4386 11.622 16.4726 11.006 17.9906 11.006H20.8286V11.82C20.8286 13.514 19.3546 14.592 17.5946 14.592Z" fill="#333333"/>
                                <path d="M31.3501 4.66996C29.4361 4.66996 28.2701 5.54996 27.7201 6.49596V4.91196H25.8941V20.202H27.7861V14.636C28.3141 15.45 29.5021 16.242 31.2841 16.242C33.8361 16.242 36.3441 14.372 36.3441 10.456C36.3441 6.56196 33.8581 4.66996 31.3501 4.66996ZM31.1081 14.526C29.0621 14.526 27.7201 12.964 27.7201 10.5V10.412C27.7201 7.94796 29.0621 6.38596 31.1081 6.38596C33.0881 6.38596 34.4081 8.03596 34.4081 10.456C34.4081 12.876 33.0881 14.526 31.1081 14.526Z" fill="#333333"/>
                                <path d="M43.3561 8.49796H40.8701V4.91196H38.9781V16H43.3341C45.7101 16 47.2501 14.372 47.2501 12.216C47.2501 10.06 45.7101 8.49796 43.3561 8.49796ZM43.1141 14.328H40.8701V10.17H43.1141C44.5661 10.17 45.3581 11.028 45.3581 12.216C45.3581 13.404 44.5661 14.328 43.1141 14.328Z" fill="#333333"/>
                                <path d="M59.1788 11.028V10.06C59.1788 6.75996 57.2868 4.66996 54.3388 4.66996C51.3028 4.66996 49.1248 6.86996 49.1248 10.456C49.1248 14.02 51.2808 16.242 54.4928 16.242C57.3748 16.242 58.7608 14.438 59.0468 13.14H57.1328C56.9348 13.712 56.0548 14.548 54.5148 14.548C52.4688 14.548 51.1048 13.052 51.1048 11.072V11.028H59.1788ZM54.2948 6.36396C56.0768 6.36396 57.1768 7.50796 57.2648 9.42196H51.1268C51.2808 7.59596 52.4468 6.36396 54.2948 6.36396Z" fill="#333333"/>
                                <path d="M67.272 4.66996C65.358 4.66996 64.192 5.54996 63.642 6.49596V4.91196H61.816V20.202H63.708V14.636C64.236 15.45 65.424 16.242 67.206 16.242C69.758 16.242 72.266 14.372 72.266 10.456C72.266 6.56196 69.78 4.66996 67.272 4.66996ZM67.03 14.526C64.984 14.526 63.642 12.964 63.642 10.5V10.412C63.642 7.94796 64.984 6.38596 67.03 6.38596C69.01 6.38596 70.33 8.03596 70.33 10.456C70.33 12.876 69.01 14.526 67.03 14.526Z" fill="#333333"/>
                                <path d="M79.058 4.66996C76.528 4.66996 74.988 6.09996 74.636 7.81596H76.462C76.77 7.00196 77.562 6.34196 79.014 6.34196C80.444 6.34196 81.522 7.11196 81.522 8.65196V9.39996H78.574C75.868 9.39996 74.218 10.698 74.218 12.832C74.218 14.944 75.89 16.242 78.046 16.242C79.454 16.242 80.774 15.714 81.61 14.68V16H83.392V8.65196C83.392 6.16596 81.654 4.66996 79.058 4.66996ZM78.288 14.592C77.056 14.592 76.132 13.976 76.132 12.81C76.132 11.622 77.166 11.006 78.684 11.006H81.522V11.82C81.522 13.514 80.048 14.592 78.288 14.592Z" fill="#333333"/>
                            </svg>
                        </div>
                        <p class="service-description">
                            Профессиональное развитие в&nbsp;IT
                        </p>
                    </a>
                    <a class="service" href="https://freelance.habr.com?utm_source=habr&utm_medium=habr_top_panel">
                        <div class="service-title">
                            <svg width="91" height="21" viewBox="0 0 91 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.032 1.65602V0.0720215H7.118V1.65602C3.268 1.72202 0.0339966 4.01002 0.0339966 8.16802C0.0339966 12.304 3.268 14.592 7.118 14.658V16.242H9.032V14.658C12.882 14.592 16.116 12.304 16.116 8.16802C16.116 4.01002 12.882 1.72202 9.032 1.65602ZM7.118 12.898C4.082 12.832 2.014 11.05 2.014 8.16802C2.014 5.26402 4.082 3.48202 7.118 3.41602V12.898ZM9.032 12.898V3.41602C12.068 3.48202 14.136 5.26402 14.136 8.16802C14.136 11.05 12.068 12.832 9.032 12.898Z" fill="#333333"/>
                                <path d="M24.2603 4.67002C22.3463 4.67002 21.1803 5.55002 20.6303 6.49602V4.91202H18.8043V20.202H20.6963V14.636C21.2243 15.45 22.4123 16.242 24.1943 16.242C26.7463 16.242 29.2543 14.372 29.2543 10.456C29.2543 6.56202 26.7683 4.67002 24.2603 4.67002ZM24.0183 14.526C21.9723 14.526 20.6303 12.964 20.6303 10.5V10.412C20.6303 7.94802 21.9723 6.38602 24.0183 6.38602C25.9983 6.38602 27.3183 8.03602 27.3183 10.456C27.3183 12.876 25.9983 14.526 24.0183 14.526Z" fill="#333333"/>
                                <path d="M39.4123 4.91202L33.7583 13.074V4.91202H31.8883V16H33.7583L39.4123 7.83802V16H41.2823V4.91202H39.4123Z" fill="#333333"/>
                                <path d="M46.0815 4.91202L45.5095 11.71C45.3555 13.624 44.9595 14.328 43.5735 14.328H43.2655V16.044H43.7935C46.0595 16.044 47.0935 14.856 47.3355 11.842L47.7755 6.60602H51.8455V16H53.7375V4.91202H46.0815Z" fill="#333333"/>
                                <path d="M61.2045 4.67002C58.6745 4.67002 57.1345 6.10002 56.7825 7.81602H58.6085C58.9165 7.00202 59.7085 6.34202 61.1605 6.34202C62.5905 6.34202 63.6685 7.11202 63.6685 8.65202V9.40002H60.7205C58.0145 9.40002 56.3645 10.698 56.3645 12.832C56.3645 14.944 58.0365 16.242 60.1925 16.242C61.6005 16.242 62.9205 15.714 63.7565 14.68V16H65.5385V8.65202C65.5385 6.16602 63.8005 4.67002 61.2045 4.67002ZM60.4345 14.592C59.2025 14.592 58.2785 13.976 58.2785 12.81C58.2785 11.622 59.3125 11.006 60.8305 11.006H63.6685V11.82C63.6685 13.514 62.1945 14.592 60.4345 14.592Z" fill="#333333"/>
                                <path d="M76.104 4.91202V9.37802H70.626V4.91202H68.734V16H70.626V11.094H76.104V16H77.996V4.91202H76.104Z" fill="#333333"/>
                                <path d="M85.9003 14.526C83.8983 14.526 82.5783 12.876 82.5783 10.456C82.5783 8.05802 83.8543 6.38602 85.8783 6.38602C87.6383 6.38602 88.4523 7.53002 88.7383 8.45402H90.6303C90.3443 6.69402 88.8263 4.67002 85.8783 4.67002C82.6883 4.67002 80.6423 7.13402 80.6423 10.456C80.6423 13.844 82.7323 16.242 85.8783 16.242C88.6283 16.242 90.3883 14.394 90.6303 12.414H88.7383C88.4963 13.36 87.7483 14.526 85.9003 14.526Z" fill="#333333"/>
                            </svg>
                        </div>
                        <p class="service-description">
                            Удаленная работа для IT-специалистов
                        </p>
                    </a>
                </div>
            </div>
            <div class="bmenu">
                <a
                        class="bmenu__conversion"
                        href="https://habr.com/sandbox/start/"
                        onclick="if (typeof ga === 'function') { ga('send', 'event', 'habr_top_panel', 'become_an_author'); }"
                >Как стать автором</a>

            </div>

            <div class="bmenu_inner" style="display:flex!important;visibility:visible!important;">
<span class="bmenu slink">
<a onclick="if (typeof ga === 'function') { ga('send', 'event', 'habr_top_panel', 'megapost', 'https://u.tmtm.ru/huawei_ag_top'); }" href="#" target="_blank" style="color: #E37979" rel=" noopener">#</a>
</span>
            </div>

        </div>
    </div>

    <script>
        var dropdown = document.querySelector('#dropdown');
        var dropdownControl = document.querySelector('#dropdown-control');
        var logoWrapper = document.querySelector('.logo-wrapper');

        document.addEventListener('click', function(e) {
            if (dropdown) {
                var isClickInside = logoWrapper.contains(e.target);
                var dropdownClosed = dropdown.classList.contains('hidden');
                if (!isClickInside && !dropdownClosed) {
                    dropdown.classList.add('hidden');
                    dropdownControl.classList.remove('reverted');
                }
            }
        });
        if (dropdownControl) {
            dropdownControl.onclick = function () {
                dropdown.classList.toggle('hidden');
                dropdownControl.classList.toggle('reverted');
            }
        }
    </script>

</div>

<div class="layout__row layout__row_navbar">
    <div class="layout__cell">
        <div class="main-navbar">
            <div class="main-navbar__section main-navbar__section_left">
                <ul class="nav-links" id="navbar-links">
                    <li class="nav-links__item">
                        <a href="https://habr.com/ru/top/" class="nav-links__item-link ">Все потоки</a>
                    </li>
                    <li class="nav-links__item">
                        <a href="https://habr.com/ru/flows/develop/" class="nav-links__item-link ">Разработка</a>
                    </li>
                    <li class="nav-links__item">
                        <a href="https://habr.com/ru/flows/admin/" class="nav-links__item-link ">Администрирование</a>
                    </li>
                    <li class="nav-links__item">
                        <a href="https://habr.com/ru/flows/design/" class="nav-links__item-link ">Дизайн</a>
                    </li>
                    <li class="nav-links__item">
                        <a href="https://habr.com/ru/flows/management/" class="nav-links__item-link ">Менеджмент</a>
                    </li>
                    <li class="nav-links__item">
                        <a href="https://habr.com/ru/flows/marketing/" class="nav-links__item-link ">Маркетинг</a>
                    </li>
                    <li class="nav-links__item">
                        <a href="https://habr.com/ru/flows/popsci/" class="nav-links__item-link ">Научпоп</a>
                    </li>
                </ul>

                <form action="https://habr.com/ru/search/#h" method="get" class="search-form" id="search-form">
                    <button type="button" class="btn btn_navbar_search icon-svg_search" id="search-form-btn" title="Поиск по сайту">
                        <svg class="icon-svg" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M17 11C17 14.3137 14.3137 17 11 17C7.68629 17 5 14.3137 5 11C5 7.68629 7.68629 5 11 5C14.3137 5 17 7.68629 17 11ZM15.5838 17.5574C14.2857 18.4665 12.7051 19 11 19C6.58172 19 3 15.4183 3 11C3 6.58172 6.58172 3 11 3C15.4183 3 19 6.58172 19 11C19 12.9998 18.2662 14.8282 17.0533 16.2307C17.0767 16.2502 17.0994 16.2709 17.1214 16.2929L20.4143 19.5858C20.8048 19.9763 20.8048 20.6095 20.4143 21C20.0238 21.3905 19.3906 21.3905 19.0001 21L15.7072 17.7071C15.6605 17.6604 15.6194 17.6102 15.5838 17.5574Z" />
                        </svg>

                    </button>
                    <label class="search-form__field-wrapper">
                        <input type="text" name="q" class="search-form__field" id="search-form-field" placeholder="Поиск" tabindex="-1"/>
                        <button type="button" class="btn btn_search-close" id="search-form-clear" title="Закрыть">
                            <svg class="icon-svg icon-svg_navbar-close-search" width="31" height="32" viewBox="0 0 31 32" aria-hidden="true" version="1.1" role="img"><path d="M26.67 0L15.217 11.448 3.77 0 0 3.77l11.447 11.45L0 26.666l3.77 3.77L15.218 18.99l11.45 11.448 3.772-3.77-11.448-11.45L30.44 3.772z"/></svg>

                        </button>
                    </label>
                </form>

            </div>

            <div class="main-navbar__section main-navbar__section_right">
                <button type="button" class="btn btn_medium btn_navbar_lang js-show_lang_settings">
                    <svg class="icon-svg" width="18" height="18">
                        <use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#globus-v2" />
                    </svg>
                </button>
                <a href="https://habr.com/ru/auth/login/" id="login" class="btn btn_medium btn_navbar_login">Войти</a>
                <a href="https://habr.com/ru/auth/register/" class="btn btn_medium btn_navbar_registration">Регистрация</a>


            </div>
        </div>

    </div>
</div>

<div class="layout__row layout__row_body">