<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

</div>

<div class="layout__row layout__row_promo-blocks">
    <div class="layout__cell">
        <div class="column-wrapper column-wrapper_bottom column-wrapper_bordered">
            <div class="content_left">



                <div class="default-block default-block_content">
                    <div class="default-block__header default-block__header_large">
                        <h2 class="default-block__header-title default-block__header-title_large">Самое читаемое</h2>
                    </div>
                    <div class="default-block__content default-block__content_most-read" id="broadcast_tabs_posts">
                        <ul class="toggle-menu toggle-menu__most-read">
                            <li class="toggle-menu__item">
                                <a href="#broadcast_posts_today" class="toggle-menu__item-link active" rel="nofollow">Сутки</a>
                            </li>
                            <li class="toggle-menu__item">
                                <a href="#broadcast_posts_week" class="toggle-menu__item-link" rel="nofollow">Неделя</a>
                            </li>
                            <li class="toggle-menu__item">
                                <a href="#broadcast_posts_month" class="toggle-menu__item-link" rel="nofollow">Месяц</a>
                            </li>
                        </ul>

                        <div class="tabs__content tabs__content_reading" id="broadcast_posts_today">
                            <ul class="content-list content-list_most-read">
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/513678/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'today'); }">Что помешало экипажу Crew Dragon выйти из корабля?</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item" title="Рейтинг">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;182</span>
                    </span>
                                        <span class="post-info__meta-item" title="Количество просмотров">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">94,5k</span>
                    </span>
                                        <span class="post-info__meta-item" title="Закладки">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">54</span>
                    </span>
                                        <a href="https://habr.com/ru/post/513678/#comments" class="post-info__meta-item" rel="nofollow" title="Комментарии">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">125</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/company/selectel/blog/514078/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'today'); }">Дождались: вышел LibreOffice 7.0</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item" title="Рейтинг">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;30</span>
                    </span>
                                        <span class="post-info__meta-item" title="Количество просмотров">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">11,3k</span>
                    </span>
                                        <span class="post-info__meta-item" title="Закладки">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">22</span>
                    </span>
                                        <a href="https://habr.com/ru/company/selectel/blog/514078/#comments" class="post-info__meta-item" rel="nofollow" title="Комментарии">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">31</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/company/mailru/blog/513968/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'today'); }">Трюки с SQL от DBA. Небанальные советы для разработчиков БД</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item" title="Рейтинг">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;55</span>
                    </span>
                                        <span class="post-info__meta-item" title="Количество просмотров">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">10,8k</span>
                    </span>
                                        <span class="post-info__meta-item" title="Закладки">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">162</span>
                    </span>
                                        <a href="https://habr.com/ru/company/mailru/blog/513968/#comments" class="post-info__meta-item" rel="nofollow" title="Комментарии">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">24</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/514044/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'today'); }">Подробный разбор стоимости жизни в Кремниевой Долине</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item" title="Рейтинг">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;26</span>
                    </span>
                                        <span class="post-info__meta-item" title="Количество просмотров">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">9,6k</span>
                    </span>
                                        <span class="post-info__meta-item" title="Закладки">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">50</span>
                    </span>
                                        <a href="https://habr.com/ru/post/514044/#comments" class="post-info__meta-item" rel="nofollow" title="Комментарии">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">40</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://u.tmtm.ru/vidcast_5" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'prmlink'); }">Вебкаст Хабр ПРО #5. Техноавтор: эксперт vs писатель</a>
                                    <div class="post-info__meta-label">
                                        Интересно
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="tabs__content tabs__content_reading" id="broadcast_posts_week">
                            <ul class="content-list content-list_most-read">
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/513678/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'week'); }">Что помешало экипажу Crew Dragon выйти из корабля?</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item" title="Рейтинг">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;182</span>
                    </span>
                                        <span class="post-info__meta-item" title="Количество просмотров">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">94,5k</span>
                    </span>
                                        <span class="post-info__meta-item" title="Закладки">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">54</span>
                    </span>
                                        <a href="https://habr.com/ru/post/513678/#comments" class="post-info__meta-item" rel="nofollow" title="Комментарии">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">125</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/513318/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'week'); }">Проект длиной в 8 лет — знал бы, ни за что не ввязался: свой 2-тактный мотор</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item" title="Рейтинг">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;237</span>
                    </span>
                                        <span class="post-info__meta-item" title="Количество просмотров">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">59,4k</span>
                    </span>
                                        <span class="post-info__meta-item" title="Закладки">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">134</span>
                    </span>
                                        <a href="https://habr.com/ru/post/513318/#comments" class="post-info__meta-item" rel="nofollow" title="Комментарии">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">84</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/513164/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'week'); }">Почтовый агент. Ловушка для жены эмигранта</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item" title="Рейтинг">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;144</span>
                    </span>
                                        <span class="post-info__meta-item" title="Количество просмотров">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">57,7k</span>
                    </span>
                                        <span class="post-info__meta-item" title="Закладки">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">71</span>
                    </span>
                                        <a href="https://habr.com/ru/post/513164/#comments" class="post-info__meta-item" rel="nofollow" title="Комментарии">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">436</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/513154/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'week'); }">«Просто похудеть» — непросто</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item" title="Рейтинг">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;152</span>
                    </span>
                                        <span class="post-info__meta-item" title="Количество просмотров">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">53,7k</span>
                    </span>
                                        <span class="post-info__meta-item" title="Закладки">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">394</span>
                    </span>
                                        <a href="https://habr.com/ru/post/513154/#comments" class="post-info__meta-item" rel="nofollow" title="Комментарии">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">506</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://u.tmtm.ru/huawei_ag_rn" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'prmlink'); }">Моё знакомство с AppGallery: как я воспользовался возможностями Huawei и нашёл точку роста для своего проекта</a>
                                    <div class="post-info__meta-label">
                                        Мегапост
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="tabs__content tabs__content_reading" id="broadcast_posts_month">
                            <ul class="content-list content-list_most-read">
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/510588/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'month'); }">Как Греф с программистами боролся</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item" title="Рейтинг">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;149</span>
                    </span>
                                        <span class="post-info__meta-item" title="Количество просмотров">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">181k</span>
                    </span>
                                        <span class="post-info__meta-item" title="Закладки">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">122</span>
                    </span>
                                        <a href="https://habr.com/ru/post/510588/#comments" class="post-info__meta-item" rel="nofollow" title="Комментарии">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">279</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/511708/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'month'); }">Web в Китае умер. Почему так произошло и что пришло вместо него?</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item" title="Рейтинг">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;15</span>
                    </span>
                                        <span class="post-info__meta-item" title="Количество просмотров">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">143k</span>
                    </span>
                                        <span class="post-info__meta-item" title="Закладки">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">133</span>
                    </span>
                                        <a href="https://habr.com/ru/post/511708/#comments" class="post-info__meta-item" rel="nofollow" title="Комментарии">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">828</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/512886/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'month'); }">USB-флешки: заряжать нельзя игнорировать</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item" title="Рейтинг">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;190</span>
                    </span>
                                        <span class="post-info__meta-item" title="Количество просмотров">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">137k</span>
                    </span>
                                        <span class="post-info__meta-item" title="Закладки">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">299</span>
                    </span>
                                        <a href="https://habr.com/ru/post/512886/#comments" class="post-info__meta-item" rel="nofollow" title="Комментарии">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">222</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/511968/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'month'); }">Коронавирус: первые итоги пандемии и карантина</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item" title="Рейтинг">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;34</span>
                    </span>
                                        <span class="post-info__meta-item" title="Количество просмотров">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">113k</span>
                    </span>
                                        <span class="post-info__meta-item" title="Закладки">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">91</span>
                    </span>
                                        <a href="https://habr.com/ru/post/511968/#comments" class="post-info__meta-item" rel="nofollow" title="Комментарии">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">646</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://u.tmtm.ru/southbridge_chitayut" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'prmlink'); }">Курсы по Docker и Kubernetes: как это сделано</a>
                                    <div class="post-info__meta-label">
                                        Интересно
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>

            <div class="sidebar">
                <div class="sidebar_right">
                    <div class="default-block">
                        <div class="default-block__header">
                            <h2 class="default-block__header-title">Партнерские материалы</h2>
                        </div>
                        <div class="default-block__content">
                            <ul class="megapost-teasers megapost-teasers_sidebar">
                                <li class="megapost-teasers__item megapost-teasers__item_sidebar teaser">
                                    <a href="https://u.tmtm.ru/huawei_ag_pb" target="_blank" class="teaser__image" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'megapost_promo', 'article_pic'); }" rel="nofollow">
                                        <img src="https://habrastorage.org/getpro/tmtm/pictures/d68/006/a14/d68006a144f493aa7aa1053be965c74b.jpg" class="teaser__image-pic"/>
                                        <div class="megapost-teasers__label">
                                            Мегапост
                                        </div>
                                    </a>
                                    <a href="https://u.tmtm.ru/huawei_ag_pb" target="_blank" class="teaser__body" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'megapost_promo', 'article_title'); }" rel="nofollow">
                                        <h3 class="teaser__body-title">Познакомился, присмотрелся и в топ: опыт работы с AppGallery</h3>
                                    </a>
                                </li>
                                <li class="megapost-teasers__item megapost-teasers__item_sidebar teaser">
                                    <a href="https://u.tmtm.ru/wifi6_promo" target="_blank" class="teaser__image" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'megapost_promo', 'article_pic'); }" rel="nofollow">
                                        <img src="https://habrastorage.org/getpro/tmtm/pictures/7b1/6bf/452/7b16bf4524ddde59b25ae7109f6f91d3.png" class="teaser__image-pic"/>
                                        <div class="megapost-teasers__label">
                                            Мегапост
                                        </div>
                                    </a>
                                    <a href="https://u.tmtm.ru/wifi6_promo" target="_blank" class="teaser__body" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'megapost_promo', 'article_title'); }" rel="nofollow">
                                        <h3 class="teaser__body-title">Как Wi-Fi 6 избавляет заводы от проводов</h3>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="default-block__footer">
                            <a href="https://tmtm.ru/megapost" class="default-block__header-link" rel="nofollow" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'megapost_promo', 'order'); }">Разместить</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

<div class="layout__row layout__row_footer-links">
    <div class="layout__cell">
        <div class="footer-grid footer-grid_menu">
            <div class="footer-grid__item footer-block">
                <h3 class="footer-block__title">
                    Ваш аккаунт
                </h3>
                <div class="footer-block__content">
                    <ul class="footer-menu">
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/auth/login/" class="footer-menu__item-link">Войти</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/auth/register/" class="footer-menu__item-link">Регистрация</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="footer-grid__item footer-block">
                <h3 class="footer-block__title">Разделы</h3>
                <div class="footer-block__content">
                    <ul class="footer-menu">
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/posts/top/" class="footer-menu__item-link">Публикации</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/news/" class="footer-menu__item-link">Новости</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/hubs/" class="footer-menu__item-link">Хабы</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/companies/" class="footer-menu__item-link">Компании</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/users/" class="footer-menu__item-link">Пользователи</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/sandbox/" class="footer-menu__item-link">Песочница</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="footer-grid__item footer-block">
                <h3 class="footer-block__title">Информация</h3>
                <div class="footer-block__content">
                    <ul class="footer-menu">
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/docs/help/" class="footer-menu__item-link">Устройство сайта</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/docs/authors/" class="footer-menu__item-link">Для авторов</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/docs/companies/" class="footer-menu__item-link">Для компаний</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/docs/docs/" class="footer-menu__item-link">Документы</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://account.habr.com/info/agreement/?hl=ru_RU" class="footer-menu__item-link">Соглашение</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://account.habr.com/info/confidential/?hl=ru_RU" class="footer-menu__item-link">Конфиденциальность</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="footer-grid__item footer-block">
                <h3 class="footer-block__title">Услуги</h3>
                <div class="footer-block__content">
                    <ul class="footer-menu">
                        <li class="footer-menu__item">
                            <a href="https://tmtm.ru/services/advertising/" target="_blank" class="footer-menu__item-link">Реклама</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://tmtm.ru/services/corpblog/" target="_blank" class="footer-menu__item-link">Тарифы</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://tmtm.ru/services/content/" target="_blank" class="footer-menu__item-link">Контент</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://tmtm.ru/workshops/" target="_blank" class="footer-menu__item-link">Семинары</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/megaprojects/" target="_blank" class="footer-menu__item-link">Мегапроекты</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://habr.market/" target="_blank" class="footer-menu__item-link">Мерч</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>


    </div>
</div>

<div class="layout__row layout__row_footer">
    <div class="layout__cell">
        <script>
            $(document).ready(function () {
                window.voteReasonsList = JSON.parse('{"1":{"id":"1","title":"Низкий технический уровень материала","order":1},"2":{"id":"2","title":"Больше рекламы, чем пользы","order":2},"3":{"id":"3","title":"Не соответствует тематике Хабра","order":3},"4":{"id":"4","title":"В тексте много ошибок и опечаток","order":4},"5":{"id":"5","title":"Пост небрежно оформлен","order":5},"6":{"id":"6","title":"Личная неприязнь к автору или компании","order":6},"7":{"id":"7","title":"В статье нет новой для меня информации","order":7},"8":{"id":"8","title":"Ничего не понял после прочтения","order":8},"22":{"id":"22","title":"Не согласен с изложенным","order":9},"9":{"id":"9","title":"Другое","order":10}}');
            });
        </script>
        <script>

            $(document).on('hljsUpdate', function () {
                if (typeof hljs === 'undefined') {
                    initHighlightJS();
                } else {
                    $('pre code').each(function (i, e) {
                        hljs.highlightBlock(e, '    ');
                    });
                }
            })

            function createScript(url) {
                var hljsScript = document.createElement("script");
                hljsScript.src = url;
                hljsScript.type = 'text/javascript';
                document.body.appendChild(hljsScript)

                return hljsScript;
            }

            function initHighlightJS() {
                if (typeof hljs !== 'undefined') {
                    $('pre code').each(function (i, e) {
                        hljs.highlightBlock(e, '    ');
                    });
                    return;
                };

                var hljsScript = createScript('https://dr.habracdn.net/habr/5f2c1748/javascripts/highlight.pack.js');

                hljsScript.onload = function() {
                    var hljsLangs = createScript('https://dr.habracdn.net/habr/5f2c1748/javascripts/highlight.langs.js');
                    hljsLangs.onload = function () {
                        hljs.initHighlighting();
                    }
                }
            }


            var codeElements = document.querySelectorAll('pre code');

            if (codeElements.length) {
                initHighlightJS();
            }

        </script>

        <script>

            var mathElements = document.getElementsByTagName('math');

            function mathJaxConfig() {
                MathJax.Hub.Config({
                    showProcessingMessages: false,
                    showMathMenu: true,
                    tex2jax: {
                        inlineMath: [['$inline$','$inline$']],
                        displayMath: [['$$display$$','$$display$$']],
                        processEscapes: true
                    },
                    MathMenu: {
                        showRenderer: true,
                        showContext:  true
                    }
                });

                MathJax.Extension.Img2jax = {
                    PreProcess: function (element) {
                        var hasMath = false;
                        var images = element.querySelectorAll('[data-tex]');
                        for (var i = images.length - 1; i >= 0; i--) {
                            var img = images[i];
                            var tex = img.alt.replace(/(\r\n|\n|\r)/gm, " ");
                            if (tex && tex[0] === '$'){
                                var script = document.createElement("script"); script.type = "math/tex";
                                hasMath = true;
                                if (img.getAttribute('data-tex') == "display"){script.type += ";mode=display"}
                                MathJax.HTML.setScript(script, tex.substring(1,tex.length-1));
                                img.parentNode.replaceChild(script,img);
                            }
                        }
                    }
                };

                MathJax.Hub.Register.PreProcessor(["PreProcess", MathJax.Extension.Img2jax]);
            }

            function mathjaxInit() {
                if (typeof MathJax === 'undefined') {
                    var url = 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS_SVG-full&locale=ru';
                    var mathScript = document.createElement("script");
                    mathScript.src = url;
                    mathScript.type = 'text/javascript';
                    document.body.appendChild(mathScript)

                    mathScript.onload = function() {
                        mathJaxConfig();
                    }
                } else {
                    mathJaxConfig();
                }
            }

            if (mathElements.length) {
                mathjaxInit();
            }

        </script>
        <div class="footer-grid footer">
            <div class="footer-grid__item footer-grid__item_copyright">
                <span class="footer__copyright">&copy; 2006 &ndash; 2020 «<a href="https://company.habr.com/" class="footer__link">Habr</a>»</span>
            </div>
            <div class="footer-grid__item footer-grid__item_link footer-grid__item_lang">
                <svg class="icon-svg icon-svg_lang-footer" width="16" height="16">
                    <use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#globus-v2" />
                </svg>
                <a href="#" class="footer__link js-show_lang_settings">Настройка языка</a>
            </div>
            <div class="footer-grid__item footer-grid__item_link">
                <a href="https://habr.com/ru/about/" class="footer__link">О сайте</a>
            </div>
            <div class="footer-grid__item footer-grid__item_link">
                <a href="https://habr.com/ru/feedback/" class="footer__link">Служба поддержки</a>
            </div>
            <div class="footer-grid__item footer-grid__item_link">
                <a href="https://m.habr.com/ru?mobile=yes" class="footer__link">Мобильная версия</a>
            </div>

            <div class="footer-grid__item footer-grid__item_social">
                <ul class="social-icons">
                    <li class="social-icons__item">
                        <a href="https://twitter.com/habr_com"
                           class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_twitter"
                           target="_blank"
                           onclick="if (typeof ga === 'function') { ga('send', 'event', 'footer', 'Social_icons', 'twitter'); }"
                        >
                            <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24"><path d="M17.414 8.642c-.398.177-.826.296-1.276.35.459-.275.811-.71.977-1.229-.43.254-.905.439-1.41.539-.405-.432-.982-.702-1.621-.702-1.227 0-2.222.994-2.222 2.222 0 .174.019.344.058.506-1.846-.093-3.484-.978-4.579-2.322-.191.328-.301.71-.301 1.117 0 .77.392 1.45.988 1.849-.363-.011-.706-.111-1.006-.278v.028c0 1.077.766 1.974 1.782 2.178-.187.051-.383.078-.586.078-.143 0-.282-.014-.418-.04.282.882 1.103 1.525 2.075 1.542-.76.596-1.718.951-2.759.951-.179 0-.356-.01-.53-.031.983.63 2.15.998 3.406.998 4.086 0 6.321-3.386 6.321-6.321l-.006-.287c.433-.314.81-.705 1.107-1.15z"/></svg>
                        </a>
                    </li>
                    <li class="social-icons__item">
                        <a href="https://www.facebook.com/habrahabr.ru"
                           class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_facebook"
                           target="_blank"
                           onclick="if (typeof ga === 'function') { ga('send', 'event', 'footer', 'Social_icons', 'facebook'); }"
                        >
                            <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24"><path d="M14.889 8.608h-1.65c-.195 0-.413.257-.413.6v1.192h2.063v1.698h-2.063v5.102h-1.948v-5.102h-1.766v-1.698h1.766v-1c0-1.434.995-2.6 2.361-2.6h1.65v1.808z"/></svg>
                        </a>
                    </li>
                    <li class="social-icons__item">
                        <a href="https://vk.com/habr"
                           class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_vkontakte"
                           target="_blank"
                           onclick="if (typeof ga === 'function') { ga('send', 'event', 'footer', 'Social_icons', 'vkontakte'); }"
                        >
                            <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24"><path d="M16.066 11.93s1.62-2.286 1.782-3.037c.054-.268-.064-.418-.343-.418h-1.406c-.322 0-.44.139-.537.343 0 0-.76 1.619-1.685 2.64-.297.33-.448.429-.612.429-.132 0-.193-.11-.193-.408v-2.607c0-.365-.043-.472-.343-.472h-2.254c-.172 0-.279.1-.279.236 0 .343.526.421.526 1.352v1.921c0 .386-.022.537-.204.537-.483 0-1.631-1.663-2.274-3.552-.129-.386-.268-.494-.633-.494h-1.406c-.204 0-.354.139-.354.343 0 .375.44 2.114 2.167 4.442 1.159 1.566 2.683 2.414 4.056 2.414.838 0 1.041-.139 1.041-.494v-1.202c0-.301.118-.429.29-.429.193 0 .534.062 1.33.848.945.901 1.01 1.276 1.525 1.276h1.578c.161 0 .311-.075.311-.343 0-.354-.462-.987-1.17-1.738-.29-.386-.762-.805-.912-.998-.215-.226-.151-.354-.001-.59z"/></svg>
                        </a>
                    </li>
                    <li class="social-icons__item">
                        <a href="https://telegram.me/habr_com"
                           class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_telegram"
                           target="_blank"
                           onclick="if (typeof ga === 'function') { ga('send', 'event', 'footer', 'Social_icons', 'telegram'); }"
                        >
                            <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24"><path d="M17.17 7.621l-10.498 3.699c-.169.059-.206.205-.006.286l2.257.904 1.338.536 6.531-4.796s.189.057.125.126l-4.68 5.062-.27.299.356.192 2.962 1.594c.173.093.397.016.447-.199.058-.254 1.691-7.29 1.728-7.447.047-.204-.087-.328-.291-.256zm-6.922 8.637c0 .147.082.188.197.084l1.694-1.522-1.891-.978v2.416z"/></svg>
                        </a>
                    </li>
                    <li class="social-icons__item">
                        <a href="https://www.youtube.com/channel/UCd_sTwKqVrweTt4oAKY5y4w"
                           class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_youtube"
                           target="_blank"
                           onclick="if (typeof ga === 'function') { ga('send', 'event', 'footer', 'Social_icons', 'youtube'); }"
                        >
                            <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="32" height="32" viewBox="0 0 32 32"><path d="M3.2 0h25.6c1.767 0 3.2 1.433 3.2 3.2v25.6c0 1.767-1.433 3.2-3.2 3.2h-25.6c-1.767 0-3.2-1.433-3.2-3.2v-25.6c0-1.767 1.433-3.2 3.2-3.2zm18.133 16l-10.667-5.333v10.667l10.667-5.333z"/></svg>

                        </a>
                    </li>
                    <li class="social-icons__item">
                        <a href="https://zen.yandex.ru/habr"
                           class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_zen"
                           target="_blank"
                           onclick="if (typeof ga === 'function') { ga('send', 'event', 'footer', 'Social_icons', 'zen'); }"
                        >
                            <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect width="30" height="30" rx="3" fill="#2C3036"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M26.25 15.1241V14.9998V14.8759C21.2684 14.7252 18.9904 14.6009 17.1735 12.8263C15.3994 11.0098 15.2747 8.73179 15.1243 3.75H15H14.8757C14.7253 8.73179 14.6006 11.0098 12.8264 12.8263C11.0095 14.6009 8.73151 14.7252 3.75 14.8759V14.9998V15.1241C8.73151 15.2748 11.0095 15.3991 12.8264 17.1733C14.6006 18.9903 14.7253 21.2682 14.8757 26.25H15H15.1243C15.2747 21.2682 15.3994 18.9903 17.1735 17.1733C18.9904 15.3991 21.2684 15.2748 26.25 15.1241Z" fill="white"/>
                            </svg>

                        </a>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</div>

<a href="#" class="layout__elevator hidden" id="scroll_to_top" title="Наверх"  onclick="if (typeof ga === 'function') { ga('send', 'event', 'navigation_button', 'down'); }">
    <svg class="icon-svg icon-svg_scroll-up" width="32" height="32" viewBox="0 0 32 32" aria-hidden="true" version="1.1" role="img"><path d="M16 0C7.164 0 0 7.164 0 16s7.164 16 16 16 16-7.164 16-16S24.836 0 16 0zm8.412 19.523c-.517.512-1.355.512-1.872 0L16 13.516l-6.54 6.01c-.518.51-1.356.51-1.873 0-.516-.513-.517-1.343 0-1.855l7.476-7.326c.517-.512 1.356-.512 1.873 0l7.476 7.327c.516.513.516 1.342 0 1.854z"/></svg>
</a>
</div>

<div class="overlay hidden" id="js-lang_settings">
    <div class="popup">
        <div class="popup__head popup__head_lang-settings">
            <span class="popup__head-title js-popup_title" data-section="1">Настройка языка</span>
            <button type="button" class="btn btn_small btn_popup-close js-hide_lang_settings">
                <svg class="icon-svg" width="12" height="12">
                    <use xlink:href="https://habr.com/5f2c1748/images/common-svg-sprite.svg#close" />
                </svg>
            </button>
        </div>
        <div class="popup__body">
            <form action="/json/settings/i18n/" method="post" class="form form_lang-settings" id="lang-settings-form">
                <fieldset class="form__fieldset form__fieldset_thin" data-section="2">
                    <legend class="form__legend form__legend_lang-settings js-popup_hl_legend">Интерфейс</legend>
                    <div class="form-field form-field_lang-settings">
              <span class="radio radio_custom ">
                <input type="radio" name="hl" id="hl_langs_ru" class="radio__input js-hl_langs" value="ru" checked>
                <label for="hl_langs_ru" class="radio__label radio__label_another">Русский</label>
              </span>
                    </div>
                    <div class="form-field form-field_lang-settings">
              <span class="radio radio_custom ">
                <input type="radio" name="hl" id="hl_langs_en" class="radio__input js-hl_langs" value="en" >
                <label for="hl_langs_en" class="radio__label radio__label_another">English</label>
              </span>
                    </div>
                </fieldset>

                <fieldset class="form__fieldset form__fieldset_thin">
                    <legend class="form__legend form__legend_lang-settings js-popup_fl_legend" data-section="3">Язык публикаций</legend>
                    <div class="form-field form-field_lang-settings">
              <span class="checkbox checkbox_custom">
                <input type="checkbox" name="fl[]" id="fl_langs_ru" class="checkbox__input js-fl_langs" value="ru" checked>
                <label for="fl_langs_ru" class="checkbox__label checkbox__label_another js-popup_feed_ru">Русский</label>
              </span>
                    </div>
                    <div class="form-field form-field_lang-settings">
              <span class="checkbox checkbox_custom">
                <input type="checkbox" name="fl[]" id="fl_langs_en" class="checkbox__input js-fl_langs" value="en" >
                <label for="fl_langs_en" class="checkbox__label checkbox__label_another js-popup_feed_en">Английский</label>
              </span>
                    </div>
                </fieldset>

                <div class="form__footer form__footer_lang-settings">
                    <button type="submit" class="btn btn_blue btn_huge btn_full-width js-popup_save_btn">Сохранить настройки</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script type="text/javascript">
    // global vars
    var g_base_url = 'habr.com/ru';
    var g_base_fullurl = 'https://habr.com/ru/';
    var g_tmid_fullurl = 'https://account.habr.com/';
    var g_is_guest = false;
    var g_show_xpanel = false;
    var g_is_enableShortcuts = '1';
    var g_is_ugc_post = '';
    var g_is_company_post = '';
    var g_current_hl = 'ru';
    var g_current_fl = 'ru';

</script>

<script src="https://dr.habracdn.net/habr/5f2c1748/javascripts/vendors.bundle.js"></script>
<script src="https://dr.habracdn.net/habr/5f2c1748/javascripts/main.bundle.js"></script>


<script src="https://dr.habracdn.net/habr/5f2c1748/javascripts/check-login.js"></script>



<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function(m,e,t,r,i,k,a){
        m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();
        k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)
    })(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(24049213, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript>
    <div>
        <img src="https://mc.yandex.ru/watch/24049213" style="position:absolute; left:-9999px;" alt="" />
    </div>
</noscript>
<!-- /Yandex.Metrika counter -->

<script type="text/javascript">
    function stripUrl(currentUrl, queryParam) {
        return currentUrl.replace(/\?([^#]*)/, function(_, search) {
            var result = search.split('&').map(function(param) {
                var regexp = new RegExp(`^${queryParam}`);
                if (regexp.test(param)) {
                    return '';
                }
                return param;
            }).filter(Boolean).join('&');
            return result ? '?' + result : '';
        });
    }

    (function() {
        if (location.search.indexOf('cv') != -1 && history.replaceState) {
            var currentUrl = location.toString();
            var strippedUrl = stripUrl(currentUrl, 'cv');
            history.replaceState({}, '', strippedUrl);
        }
    })();

    function callGA(usesABP) {
        if (typeof window.adb1 === 'undefined') { window.adb1 = 'yes';}
        if (usesABP) { window.adb1 = 'aa'; }

        var user_type = 'guest';

        var page_type = "other";

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-726094-1',  'auto');
        ga('create', 'UA-726094-24', 'auto', {'name': 'HGM', 'allowLinker': true});

        ga('HGM.require', 'linker');
        ga('HGM.linker:autoLink', ['toster.ru', 'habr.com', 'account.habr.com', 'm.habr.com']);

        ga('require', 'displayfeatures');
        ga('set', 'dimension1', user_type); // user type - guest/readonly/habrauser
        ga('set', 'dimension2', page_type);
        ga('set', 'dimension3', 'na');
        ga('set', 'dimension4', window.adb1);
        ga('set', 'dimension6', 'ru');
        ga('set', 'dimension7', 'A');


        (function() {
            var removeUtms = function(){
                var location = window.location;
                if (location.search.indexOf('utm_') != -1 && history.replaceState) {
                    var currentUrl = location.toString();
                    var strippedUrl = stripUrl(currentUrl, 'utm_');
                    history.replaceState({}, '', strippedUrl);
                }
            };
            ga('require', 'GTM-559GVC8');
            ga('send', 'pageview', { 'hitCallback': removeUtms });
        })();

        ga('HGM.set', 'dimension1', user_type);
        ga('HGM.set', 'dimension2', "habrahabr");
        ga('HGM.set', 'dimension4', window.adb1);

        ga('HGM.send', 'pageview');
    }


    if (window.habr_blockers_checker) {
        window.habr_blockers_checker.detectWrapper(callGA);
    } else {
        callGA(false)
    }

</script>

<!-- Facebook Pixel Code -->
<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=317458588730613&ev=PageView&noscript=1"/>
<!-- End Facebook Pixel Code -->

<img src="https://vk.com/rtrg?p=VK-RTRG-421343-57vKE" style="position:fixed; left:-999px;" alt=""/>

<? popups()->printAll() ?>
<? $APPLICATION->ShowViewContent('scripts') ?>

</body>
</html>