<?php

namespace App\Components;

use App\Models\User;
use App\Models\Post;
use \CBitrixComponent;

/**
 * Class PostsListComponent
 * @package App\Components
 * @property array $arParams
 * @property array $arResult
 */
class PostsListComponent extends BaseComponent
{
    public function executeComponent()
    {
        if(isset($_REQUEST['param'])) {
            $ratingLimit = $_REQUEST['param'];
            if(in_array($ratingLimit, [10, 25, 50, 100])) {

                $posts = Post::filter(array('>=PROPERTY_RATING' => $ratingLimit))->with('user')->fetchUsing('GetNext')->paginate($this->arParams["PAGINATION"]);

                $this->arResult['RATING_LIMIT'] = $ratingLimit;
            }
        }

        if(!isset($posts)) {

            $posts = Post::with('user')->paginate($this->arParams["PAGINATION"]);
        }

        $this->arResult['POSTS'] = $posts;

        $this->includeComponentTemplate();
    }

}

