<?php

namespace App\Components;

use App\Models\User;
use App\Models\Post;

/**
 * Class PostsComponent
 * @package App\Components
 * @property array $arParams
 * @property array $arResult
 */
class PostsComponent extends BaseComponent
{
    public function executeComponent()
    {
        if(isset($_REQUEST['postId'])) {

            $this->includeComponentTemplate('detail');

            return;
        }

        $this->IncludeComponentTemplate('list');

        return;
    }

}