<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
    "GROUPS" => array(
        "LIST_SETTINGS" => array(
            "NAME" => "Настройки списка публикаций"
        )
    ),
    "PARAMETERS" => array(
        "PAGINATION" => array(
            "PARENT" => "LIST_SETTINGS",
            "NAME" => "Количество постов на странице",
            "TYPE" => "STRING",
            "DEFAULT" => "5",
        ),
        "SEF_MODE" => array(
            'list' => array(
                'NAME' => 'Главная страница',
                'DEFAULT' => '',
            ),
            'detail' => array(
                'NAME' => 'Страница элемента',
                'DEFAULT' => 'post/',
            ),
        ),
    ),
);
