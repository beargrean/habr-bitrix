<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = [
    "NAME" => "Публикации",
    "DESCRIPTION" => "",
    "PATH" => [
        "ID" => "main",
        "CHILD" => [
            "ID" => "habr_components"
        ]
    ],
];
