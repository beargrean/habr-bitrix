$.ajaxSetup({
    data: {csrf_token: "<?= bitrix_sessid() ?>"},
    beforeSend: function (xhr, settings) {
        if (settings.url.startsWith('/api/')) {
            xhr.setRequestHeader('X-Language-Id', '<?= LANGUAGE_ID ?>');
        }
    }
});

function posts_vote(element) {
    $.ajax({
        url: `/api/internal/vote/${$(element).attr('post-id')}/`,
        type: 'PATCH',
        dataType: 'json',
        data: {
            vote: $(element).attr('data-action')
        },
        beforeSend: function () {
            $(element).prop('disabled', true);
        },
        success: function (data) {
            console.log(data);
            posts_vote_result(data.plusVotes, data.minusVotes);

            function posts_vote_result(plusVotes, minusVotes) {
                $('#vote_result').attr('title',`Всего голосов ${plusVotes + minusVotes}: ↑${plusVotes} и ↓${minusVotes}`);
                if ((plusVotes - minusVotes) > 0) {
                    $('#vote_result').text(`+${plusVotes - minusVotes}`);
                } else {
                    $('#vote_result').text(`${plusVotes - minusVotes}`);
                }
            }
            $(element).prop('disabled', false);
        },
        error: function (message) {
            alert("Голос не учтён ", message);
            $(element).prop('disabled', false);
        }
    });
}