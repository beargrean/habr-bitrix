<?php

namespace App\Components;

use App\Models\User;
use App\Models\Post;

/**
 * Class PostDetailComponent
 * @package App\Components
 * @property array $arParams
 * @property array $arResult
 */
class PostDetailComponent extends BaseComponent
{
    public function executeComponent()
    {
        $this->arResult = Post::with('user')->getById($_REQUEST['postId']);

        if(!$this->arResult) {
            LocalRedirect("/404");
        }

        $this->includeComponentTemplate();
    }

}

