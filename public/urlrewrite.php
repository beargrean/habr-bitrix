<?php
$arUrlRewrite = array(
    0 => array(
        'CONDITION' => '#^/bitrix/services/ymarket/#',
        'RULE' => '',
        'ID' => '',
        'PATH' => '/bitrix/services/ymarket/index.php',
        'SORT' => 100,
    ),
    array(
        "CONDITION" => "#^/post/([0-9]+)#",
        "RULE" => "postId=$1",
        "PATH" => "/index.php",
    ),
    array(
        "CONDITION" => "#^/top([0-9]+)$#",
        "RULE" => "param=$1",
        "PATH" => "/index.php",
    ),

);
